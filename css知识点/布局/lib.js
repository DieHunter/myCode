// 数组循环
const arrayLoop = (list, current = 0) => ({
  item: list[current],
  current: (current + 1) % list.length,
});
// 创建标签
const createElement = ({ ele, style, attr, parent }) => {
  const element =
    ele instanceof HTMLElement ? ele : document.createElement(ele ?? "div");
  style &&
    Object.keys(style)?.forEach((key) => (element.style[key] = style[key]));
  attr && Object.keys(attr)?.forEach((key) => (element[key] = attr[key]));
  parent && parent.appendChild(element);
  return element;
};

