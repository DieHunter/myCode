const App = require("koa");
const { koaBody } = require("koa-body");
const app = new App();
const koaStatic = require("koa-static");
const path = require("path");
const temp = "./temp";

const p = path.join(__dirname, temp);

// 中间件处理跨域
app.use(async (ctx, next) => {
  // 设置跨域响应头
  ctx.set("Access-Control-Allow-Origin", "*");
  ctx.set("Access-Control-Allow-Methods", "POST, OPTIONS, GET");
  ctx.set("Access-Control-Allow-Headers", "Content-Type, Authorization");
  // 处理OPTIONS预检
  if (ctx.method === "OPTIONS") {
    ctx.status = 204;
  } else {
    await next();
  }
});
// 创建静态资源路由
app.use(koaStatic(p));
// 文件上传读取body
app.use(
  koaBody({
    multipart: true, // 启用文件上传
    formidable: {
      uploadDir: p, // 设置文件上传目录
      keepExtensions: true, // 保持文件扩展名
      filename: (name, ext) => {
        // 自定义文件名
        return name + ext;
      },
    },
  })
);
app.use((ctx, next) => {
  // @ts-ignore
  const newFilename = ctx.request?.files?.file.newFilename;
  console.log(newFilename, new Date());
  ctx.response.body = { msg: "上传成功", newFilename };
  next();
});

app.listen(1024, () => {
  console.log("服务启动");
});
