const fileElem = document.getElementById("file");
const url = "http://localhost:1024/";
let fileUrl = "";
fileElem && (fileElem.onchange = fileReader);
function preview() {
  if (!fileUrl) return console.log("请先上传文件");
  window.open(url + fileUrl);
}
function fileReader(e) {
  console.log(e.target.files[0]);
  upload(e.target.files[0]);
}
function upload(file) {
  const formData = new FormData();
  formData.append("file", file);
  fetch(url, {
    method: "POST",
    body: formData,
  })
    .then((res) => res.json())
    .then((res) => {
      console.log(res);
      fileUrl = res.newFilename;
    });
}

function download() {
  if (!fileUrl) return console.log("请先上传文件");
  fetch(url + fileUrl)
    .then((response) => response.blob())
    .then((blob) => {
      // 模拟下载操作
      const link = document.createElement("a");
      link.href = URL.createObjectURL(blob);
      link.download = fileUrl;
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    })
    .catch((error) => console.error("下载出错:", error));
}

async function filePicker() {
  // 打开文件选择器并从结果中解构出第一个句柄
  const [fileHandle] = await window.showOpenFilePicker();
  const file = await fileHandle.getFile();
  return file;
}

function dragFile(elem, handle) {
  // 阻止默认行为
  ["dragenter", "dragover", "dragleave", "drop"].forEach((eventName) => {
    elem.addEventListener(eventName, preventDefaults, false);
    document.body.addEventListener(eventName, preventDefaults, false);
  });
  function preventDefaults(e) {
    e.preventDefault();
    e.stopPropagation();
  }
  // 添加拖放区高亮效果
  ["dragenter", "dragover"].forEach((eventName) => {
    elem.addEventListener(
      eventName,
      () => elem.classList.add("dragging"),
      false
    );
  });
  ["dragleave", "drop"].forEach((eventName) => {
    elem.addEventListener(
      eventName,
      () => elem.classList.remove("dragging"),
      false
    );
  });
  // 处理拖放事件
  elem.addEventListener("drop", handle, false);
}
// 延时函数
const sleep = (delay = 1000) =>
  new Promise((resolve) => setTimeout(resolve, delay));

class FileChunk {
  constructor(chunkSize = 1024 * 1024 * 1) {
    this.chunkSize = chunkSize;
    this.currentChunkIndex = 0; // 当前上传的块索引
    this.totalChunks = 0; // 总块数
  }
  start(file) {
    this.file = file;
    const { chunkSize } = this;
    this.currentChunkIndex = 0;
    this.totalChunks = Math.ceil(file.size / chunkSize);
    return this.createChunkList(file, chunkSize);
  }
  createChunk(current, file, chunkSize) {
    const chunkObj = {
      id: current,
      start: current * chunkSize,
    };
    chunkObj.end = Math.min(file.size, chunkObj.start + chunkSize);
    chunkObj.chunk = file.slice(chunkObj.start, chunkObj.end);
    return chunkObj;
  }
  createChunkList(file, chunkSize) {
    const chunkList = [];
    const { totalChunks } = this;
    while (this.currentChunkIndex < totalChunks) {
      chunkList.push(
        this.createChunk(this.currentChunkIndex++, file, chunkSize)
      );
    }
    return chunkList;
  }
}

function updateProgress(progress) {
  const progressBar = document.querySelector(".progress-bar");
  progressBar.style.width = `${progress}%`;
}
