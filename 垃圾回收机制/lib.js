// 标记清除, 垃圾回收机制
class MarkGC {
  marked = new Set(); // 模拟标记操作
  run(obj) {
    this.marked.clear(); // 这一步应该是放在最后的，但是看不出效果，所以改成运行前重置
    this.mark(obj);
    this.sweep(obj); // 这一步实际上没有效果，为了方便理解
    return this;
  }
  //   判断对象或属性是否已经标记
  checkMark = (obj) => typeof obj === "object" && !this.marked.has(obj);
  mark(obj) {
    const { marked } = this;
    if (this.checkMark(obj)) {
      marked.add(obj);
      Reflect.ownKeys(obj).forEach((key) => this.mark(obj[key]));
    }
  }
  sweep(obj) {
    Reflect.ownKeys(obj).forEach((key) => {
      const it = obj[key];
      if (this.checkMark(it)) {
        delete obj[key];
        this.sweep(it);
      }
    });
  }
}

// 引用计数器
class RefCount {
  constructor() {
    this.count = 0;
  }

  increment() {
    this.count++;
  }

  decrement() {
    this.count--;
  }
}

// 对象类
class MyObject {
  constructor() {
    this.refCount = new RefCount();
    this.refCount.increment(); // 对象被创建时，引用计数加1
  }

  addReference() {
    this.refCount.increment(); // 引用增加时，引用计数加1
  }

  releaseReference() {
    this.refCount.decrement(); // 引用减少时，引用计数减1
    if (this.refCount.count === 0) {
      this.cleanup(); // 引用计数为0时，进行清理操作
    }
  }

  cleanup() {
    // 执行清理操作，释放资源
    console.log("清理完成");
  }
}
// 新生代回收机制
class GenerationalCollection {
  // 定义堆的From空间和To空间
  fromSpace = new Set();
  toSpace = new Set();
  garbageCollect(obj) {
    this.mark(obj); // 标记阶段
    this.sweep(); // 清除阶段
    // 切换From和To的空间
    const { to, from } = this.exchangeSet(this.fromSpace, this.toSpace);
    this.fromSpace = from;
    this.toSpace = to;
    return this;
  }
  isObj = (obj) => typeof obj === "object";
  exchangeSet(from, to) {
    from.forEach((it) => {
      to.add(it);
      from.delete(it);
    });
    return { from, to };
  }
  allocate(obj) {
    this.fromSpace.add(obj);
  }
  mark(obj) {
    if (!this.isObj(obj) || obj?.marked) return;
    obj.marked = true;
    this.isObj(obj) &&
      Reflect.ownKeys(obj).forEach((key) => this.mark(obj[key]));
  }
  sweep() {
    const { fromSpace, toSpace } = this;
    fromSpace.forEach((it) => {
      if (it.marked) {
        // 将标记对象放到To空间
        toSpace.add(it);
      }
      // 从From空间中移除该对象
      fromSpace.delete(it);
    });
  }
}
