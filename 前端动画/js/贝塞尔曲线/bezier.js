// 二次贝塞尔
function quadraticBezier(_x, _y, t) {
  const mt = 1 - t;
  const t2 = t * t;
  const x = 2 * mt * t * _x + t2;
  const y = 2 * mt * t * _y + t2;
  return [x, y];
}

// 三次贝塞尔
function cubicBezier(_x1, _y1, _x2, _y2, t) {
  const cx = 3 * _x1;
  const cy = 3 * _y1;
  const bx = 3 * (_x2 - _x1) - cx;
  const by = 3 * (_y2 - _y1) - cy;
  const ax = 1 - cx - bx;
  const ay = 1 - cy - by;
  const x = ax * Math.pow(t, 3) + bx * Math.pow(t, 2) + cx * t;
  const y = ay * Math.pow(t, 3) + by * Math.pow(t, 2) + cy * t;
  return [x, y];
}
// 计算阶乘
function factorial(n) {
  if (n === 0 || n === 1) {
    return 1;
  }
  return n * factorial(n - 1);
}
// 计算组合数
function combination(n, k) {
  return factorial(n) / (factorial(k) * factorial(n - k));
}
// N次贝塞尔，x和y坐标使用二阶数组传递
function NBezier(points, t) {
  const n = points.length - 1;
  const result = [0, 0];
  for (let i = 0; i <= n; i++) {
    const coefficient =
      combination(n, i) * Math.pow(1 - t, n - i) * Math.pow(t, i);
    result[0] += coefficient * points[i][0];
    result[1] += coefficient * points[i][1];
  }
  return result;
}
