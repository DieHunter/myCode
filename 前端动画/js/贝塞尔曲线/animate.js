class Animate {
  id = null; // 动画索引
  duration = Infinity; // 帧数控制
  isActive = false; // 激活控制
  constructor(fn) {
    this.fn = fn;
  }
  start(duration) {
    if (this.isActive) return;
    this.duration = duration ?? Infinity;
    this.isActive = true;
    this.animate();
  }
  stop() {
    this.isActive = false;
    cancelAnimationFrame(this.id);
  }
  animate(timer) {
    if (this.isActive && this.duration-- > 0) {
      this.fn(timer);
      this.id = requestAnimationFrame(this.animate.bind(this));
    }
  }
}
