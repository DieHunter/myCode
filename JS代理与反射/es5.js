function __Proxy(target, handler) {
  var __target = {};
  this.target = target;
  this.handler = handler;
  this.init(__target);
  return __target;
}
__Proxy.prototype = {
  init(__target) {
    this.readWrite(__target);
    Object.prototype.__defineProperty = this.defineProperty.bind(this);
    Object.prototype.__delete = this.deleteProperty.bind(this);
  },
  readWrite(__target) {
    // 初始化读写函数
    var target = this.target;
    var handler = this.handler;
    for (const key in target) {
      Object.defineProperty(__target, key, {
        configurable: true,
        set(val) {
          // 新增/修改
          target[key] = val;
          return handler.set(target, key, val);
        },
        get() {
          // 读取
          return handler.get(target, key);
        },
      });
    }
  },
  defineProperty(target, property, descriptor) {
    // 定义/修改
    var __d = this.handler.defineProperty;
    var fn = typeof __d === "function" ? __d : __Reflect.defineProperty; // 如果钩子函数存在，则执行代理拦截函数
    return fn(target, property, descriptor);
  },
  deleteProperty(target, property) {
    // 删除
    var __delete = this.handler.deleteProperty;
    if (typeof __delete === "function") {
      return __delete(target, property); // 如果钩子函数存在，则执行代理拦截函数
    }
    return __Reflect.deleteProperty(target, property);
  },
};

var __Reflect = {
  set(target, prop, value) {
    target[prop] = value;
  },
  get(target, prop) {
    return target[prop];
  },
  defineProperty(target, property, descriptor) {
    return Object.defineProperty(target, property, descriptor);
  },
  deleteProperty(target, property) {
    return delete target[property];
  },
};
var obj = {
  name: "张三",
  age: 20,
};

var proxy = new __Proxy(obj, {
  set(target, prop, value) {
    console.log("set");
    return __Reflect.set(target, prop, value);
  },
  get(target, prop) {
    console.log("get");
    return __Reflect.get(target, prop);
  },
  defineProperty(target, property, descriptor) {
    console.log("defineProperty");
    return __Reflect.defineProperty(target, property, descriptor);
  },
  deleteProperty(target, property) {
    console.log("deleteProperty");
    return __Reflect.deleteProperty(target, property);
  },
});
// set
proxy.name = "李四";
// get
console.log(proxy.name);
// defineProperty
Object.__defineProperty(proxy, "name", {
  value: "王五",
});// 使用自定义的defineProperty，劫持对象操作
// delete
Object.__delete(proxy, "name");// 这个函数是模拟delete关键字的操作
console.log(proxy.name, obj.name);
