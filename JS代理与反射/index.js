const proxyFactory = (target, opts) => {
  return new Proxy(target, {
    set: (target, propertyKey, value, receiver) => {
      // console.log(target, propertyKey, value, receiver);
      console.log("执行了set");
      return Reflect.set(target, propertyKey, value);
    },
    get: (target, property, receiver) => {
      // console.log(target, property, receiver);
      console.log("执行了get");
      return Reflect.get(target, property, receiver);
    },
    has: (target, property) => {
      // console.log(target, property);
      console.log("执行了has");
      return Reflect.has(target, property);
    },
    deleteProperty: (target, property) => {
      // console.log(target, property);
      console.log("执行了deleteProperty");
      return Reflect.deleteProperty(target, property);
    },
    apply: (target, thisArg, argumentsList) => {
      // console.log(target, thisArg, argumentsList);
      console.log("执行了apply");
      return Reflect.apply(target, thisArg, argumentsList);
    },
    construct: (target, argumentsList, newTarget) => {
      // console.log(target, argumentsList, newTarget);
      console.log("执行了construct");
      return Reflect.construct(target, argumentsList, newTarget);
    },
    getOwnPropertyDescriptor: (target, property) => {
      // console.log(target, property);
      console.log("执行了getOwnPropertyDescriptor");
      return Reflect.getOwnPropertyDescriptor(target, property);
    },
    defineProperty: (target, property, descriptor) => {
      // console.log(target, property, descriptor);
      console.log("执行了defineProperty");
      return Reflect.defineProperty(target, property, descriptor);
    },
    getPrototypeOf: (target) => {
      // console.log(target);
      console.log("执行了getPrototypeOf");
      return Reflect.getPrototypeOf(target);
    },
    setPrototypeOf: (target, prototype) => {
      // console.log(target, prototype);
      console.log("执行了setPrototypeOf");
      return Reflect.setPrototypeOf(target, prototype);
    },
    isExtensible: (target) => {
      // console.log(target);
      console.log("执行了isExtensible");
      return Reflect.isExtensible(target);
    },
    preventExtensions: (target) => {
      // console.log(target);
      console.log("执行了preventExtensions");
      return Reflect.preventExtensions(target);
    },
    ownKeys: (target) => {
      // console.log(target);
      console.log("执行了ownKeys");
      return Reflect.ownKeys(target);
    },
    ...opts,
  });
};

const obj = {
  name: "张三",
  age: 20,
};
const fn = function () {
  return "hello";
};
const __obj = proxyFactory(obj);
const __fn = proxyFactory(fn); // apply只有当当前代理对象为函数时才会执行
export const init = () => {
  // set;
  __obj.name = "李四";
  // get;
  __obj.name;
  // has;
  "name" in __obj;
  // deleteProperty;
  delete __obj.age;
  // apply;
  __fn();
  // construct;
  new __fn();
  // getOwnPropertyDescriptor;
  Object.getOwnPropertyDescriptor(__obj, "name");
  // defineProperty;
  Object.defineProperty(__obj, "name", {
    value: "王五",
  });
  // getPrototypeOf;
  Object.getPrototypeOf(__obj);
  // setPrototypeOf;
  Object.setPrototypeOf(__obj, null);
  // isExtensible;
  Object.isExtensible(__obj);
  // preventExtensions;
  Object.preventExtensions(__obj);
  // ownKeys;
  Object.getOwnPropertyNames(__obj)
  console.log(__obj, obj);
};
