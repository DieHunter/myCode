## Top Level（顶层配置）

与 Compiler Options 在同一级

files：这个选项允许你显式列出需要包含在项目中的文件列表。如果你有一些特定的文件是项目的一部分，并且你想要确保它们被 TypeScript 编译器考虑在内，那么就应该将它们添加到这个列表中。
extends：通过这个选项，你的 tsconfig.json 可以继承另一个配置文件中的设置。这允许你创建一个共享的基础配置文件，并且在多个项目之间复用。继承的配置文件路径是相对于当前 tsconfig.json 的。
include：类似于 files，这个选项允许你指定一个文件模式的列表，用于匹配需要包括在编译上下文中的文件。这些模式支持通配符，因此提供了更弹性的文件选择方式。
exclude：这个选项用于指定编译过程中应该被排除的文件或文件夹。任何与提供的文件模式匹配的文件都将不被包括在编译中。如果 include 选项也被指定了，exclude 选项将对其结果进行过滤。
references：这是用于设置项目引用的一个数组。项目引用允许你将 TypeScript 项目分解成较小的项目，这些小项目可以单独编译，并且可以被其他 TypeScript 项目引用。每个引用需指定一个 path 属性，该属性是对另一个 tsconfig.json 文件的引用。

## Compiler Options (编译器选项)

编译器的配置选项，控制 TypeScript 编译过程

### Type Checking（类型检查）

类型检查配置，确保代码的类型安全性。

allowUnreachableCode：此选项用于控制是否允许代码中出现无法到达的代码块。在 ts 文件中，如果设置为 true，TypeScript 编译器会忽略那些永远不会执行到的代码，例如在 return 语句后面的代码。如果设置为 false（默认值），则编译器会对这样的代码抛出警告。
allowUnusedLabels：设定是否允许在代码中出现未使用的标签。在 ts 文件中，将此设为 true，您可以定义标签（例如在循环前使用标签）而不使用它们，编译器不会给出警告。如果设为 false（默认值），未使用的标签会导致编译器警告。
alwaysStrict：该选项强制所有 ts 文件以严格模式运行，相当于在所有 ts 文件顶部自动加上了  "use strict"; 指令。这将使解析器在更严格的条件下运行，诸如不允许未声明的变量等。
exactOptionalPropertyTypes：此选项要求可选属性类型必须明确指定其类型。在 ts 文件中使用时，例如，你不能将一个可能是 undefined 的属性赋值给仅声明了特定类型的可选属性。如果设置为 true，这将强制要求额外的类型准确性。
noFallthroughCasesInSwitch：当此选项设置为 true 时，它会防止 switch 语句中的 case 无意间"贯穿"到下一个 case。它要求每个 case 后面都必须有一个 break，return 或 throw 语句，否则编译器会抛出错误。
noImplicitAny：这个选项设置为 true 时会防止将变量隐式地设为 any 类型。这意味着你必须为每个变量、参数和函数返回值明确指定类型，否则 TypeScript 编译器会抛出错误。
noImplicitOverride：开启这个选项要求在子类中重写父类方法时必须明确使用 override 关键字，这有利于提高代码的可读性和减少误写的错误。
noImplicitReturns：当该选项设置为 true 时，它确保函数的所有代码分支都有返回值。这意味着如果你的函数声明返回了一个类型，那么所有分支都必须遵守这一点，否则会报错。
noImplicitThis：启用这个选项后，this 关键词在不明确类型的上下文中将会被认为是错误的。换句话说，你需要明确 this 的类型以防止编译器错误。
noPropertyAccessFromIndexSignature：设置此项为 true 将禁止通过索引签名以点（.）方式访问属性。这意味着如果你有一个带索引签名的对象，你必须使用括号访问法来访问那些属性。
noUncheckedIndexedAccess：此选项开启后，对索引类型的访问将被认为是不确定的，即可能是 undefined。这确保了在访问像数组一类的索引类型时，必须考虑到未定义值的可能性。
noUnusedLocals：开启这个选项会在本地作用域中声明了变量但未使用时给出错误。这有助于保持代码的清洁，避免冗余。
noUnusedParameters：类似地，如果你在函数中定义了参数但在函数体内没有使用，开启此选项会导致编译器错误。
strict：这是一个开关型选项，将其设置为 true 将同时启用多个严格检查的模式，包括上面提到的 noImplicitAny、noImplicitThis、strictNullChecks 等等，基本上是强类型检查的严格模式。
strictBindCallApply：设置此选项后，bind、call 和 apply 方法必须传递正确类型的参数，否则会导致编译错误。
strictFunctionTypes：开启此选项会对函数类型赋值进行更严格的检查，特别是在处理函数参数类型与返回类型的兼容性时。
strictNullChecks：该选项开启时，null 和 undefined 每个类型都会被视为不同。这要求你在可能出现 null 或 undefined 的情况下进行显式检查。
strictPropertyInitialization：此选项确保每个类的属性都必须声明时就初始化，或者在构造函数中初始化，否则编译器会抛出错误。
useUnknownInCatchVariables：如果开启这个选项，那么 catch 块中的异常变量将使用 unknown 类型而不是 any，这意味着你必须在对其进行任何操作之前对它进行类型检查。这有利于减少运行时错误。

### Modules（模块）

模块系统的配置，管理代码的组织结构

​​​​allowUmdGlobalAccess：这个设置允许你在模块中访问 UMD 全局。也就是说，即使你处于一个模块环境中（例如使用了 import 或者 export），你仍然能够访问那些用 UMD 格式定义的全局变量。
baseUrl：baseUrl 是用来设置非相对模块的基准路径。例如，如果你设置了"baseUrl": "./src"，那么当你使用像 import _ as utils from "utils"这样的导入语句时，TypeScript 编译器会从 src 文件夹开始查找 utils。
customConditions：该配置项用于定义在 import 或 export 时解析模块路径的自定义条件，通常与条件性导出相关。
module：此选项用于指定要使用的模块代码生成方式。比如"module": "commonjs"将生成适用于 Node.js 的 CommonJS 模块，而"module": "esnext"将会输出 ES6 模块，以适配更现代的 JavaScript 环境。
moduleResolution：这个配置项决定了 TypeScript 如何解析模块。你可以设置它为"node"以使用 Node.js 的模块解析策略，或是"classic"来使用 TypeScript 旧的模块解析方式。
moduleSuffixes：这个选项可以让你指定在解析模块时要附加到模块名称上的字符串数组，以此来控制模块的解析策略。
noResolve：设置为 true 时，TypeScript 编译器将不会尝试解析或加载外部模块和声明文件的 import 声明。这通常用于调试模块解析问题。
paths：此选项用于设置一个路径映射，用于导入时的重定向。例如，你可以映射"components/_": ["./src/components/*"]，这样在你的 ts 文件中通过 import { Button } from "components/button"会被重定向到 src / components / button。
resolveJsonModule：当此选项开启时，TypeScript 允许你导入 JSON 文件作为模块。这意味着你可以通过标准模块导入语句来获取 JSON 文件中的数据。
resolvePackageJsonExports：与上述 customConditions 类似，该配置允许解析符合 package.json 中 exports 字段的模块路径。
resolvePackageJsonImports：此设置在解析 import 语句时允许对 package.json 文件中的 imports 字段进行解析。
rootDir：该选项指定输入文件的根目录。这通常用于控制输出目录结构，编译器会以此目录为基础生成输出文件的结构。
rootDirs：类似于 rootDir，rootDirs 是一个路径的列表，TypeScript 编译器会将这些目录虚拟地合并在一起，就好像它们是在一个文件夹中一样。
typeRoots：这个选项允许你指定用于声明文件的文件夹列表。TypeScript 将只会包含这些路径中的 \*.d.ts 文件，这允许你有更细粒度的控制哪些类型声明文件被包含进来。
types：此设置用于指定需要包含的类型声明文件。它允许你列出所有期望加载的 @types 包的名称数组。如果这个列表是空的，所有可见的 @types 包将被包含进来。

### Emit（发出）

控制 TypeScript 的输出、生成文件

declaration：当这个选项设置为 true 时，TypeScript 编译器会为每个 ts 文件生成.d.ts 文件。这些声明文件作为 TypeScript 代码暴露给其他项目的类型定义，它们不包含具体的实现。
declarationDir：此选项用于指定声明文件(.d.ts)的输出目录。你可以指定一个专门的文件夹来存放这些生成的类型声明文件。
declarationMap：如果启用，编译器会为每个生成的声明文件(.d.ts)同时生成对应的源码映射文件(.map)。这些文件有助于调试器根据声明文件映射回原始源码。
downlevelIteration：当这个选项为 true 时，TypeScript 将会使用兼容旧版本 JavaScript 的方式来编译 for：of 循环、展开操作符等迭代器相关语法。
emitBOM：设置这个值为 true 时，会在输出文件的开始处加入一个字节顺序标记(Byte Order Mark)。这可以影响文件在某些编辑器或者操作系统中的显示。
emitDeclarationOnly：如果这个选项为 true，编译器只会生成声明文件(.d.ts)，而不会生成 JavaScript 代码文件。
importHelpers：该选项为 true 时，编译器会从 TypeScript 的 tslib 库中导入辅助函数，例如**extends，**rest 等，而不是在每个文件中重复它们，以减小输出文件的大小。
importsNotUsedAsValues：这个选项用于控制那些只为了类型而导入，但在运行时不会用到的 import 语句。这可以是"remove"，表示完全移除这些导入，或者是"preserve"，表示保留这些导入不变。
inlineSourceMap：当设置为 true 时，源代码映射将被直接嵌入到输出的 JavaScript 文件中，而不是生成一个单独的.map 文件。
inlineSources：这个选项一同启用时，源代码本身也会被嵌入到内联的 source map 中。
mapRoot：这个选项允许你指定放置 source map 文件的根目录。
newLine：用于指定输出文件中的换行符类型。"CRLF"用于 Windows，而"LF"用于 Unix / Linux。
noEmit：设置为 true 时，编译器不会输出文件。这仅用于在编译过程中检查错误。
noEmitHelpers：如果这个选项设置为 true，编译器不会生成在文件顶部的辅助函数代码。这通常与 importHelpers 配合使用。
noEmitOnError：该选项为 true 时，则只有当没有任何错误时，编译器才会输出文件。
outDir：指定编译器输出文件的目录。所有产出的 JavaScript 文件都会放在这个目录下。
outFile：如果设置了此选项，所有的全局（非模块）文件会被合并并输出到一个单一的文件中。
preserveConstEnums：当设置为 true 时，编译器会保留 const enum 声明，而不是将它们替换成对应的内联代码。
preserveValueImports：与 importsNotUsedAsValues 类似，该选项的确定是为了指导 TypeScript 如何处理只被使用其类型的导入。
removeComments：设为 true 则在输出文件中移除所有注释，通常用于生产环境的代码清理。
sourceMap：启用此选项，编译器将为每个文件生成一个相应的.map 文件。这些文件有助于开发者在代码调试时映射回原始的 TypeScript 代码。
sourceRoot：这个选项指定了源映射中的源文件位置，用于调试器定位原始源文件。
stripInternal：这个选项为 true 时，那些在声明文件中用/\*_ @internal _/注释的代码会在输出的.d.ts 文件中被省略。

### JavaScript Support（JS 支持）

配置 TypeScript 对现有 JavaScript 代码的支持

allowJs：这个选项设置为 true 时，TypeScript 编译器会允许 JavaScript 文件（.js 扩展名）作为输入文件进行编译。这允许你在 TypeScript 项目中混合使用 JavaScript 和 TypeScript 文件。
checkJs：当你开启这个配置项时，TypeScript 编译器同样会检查 JavaScript 文件中的错误。这就像在你的 JavaScript 文件上运行了 TypeScript 的类型检查器。
maxNodeModuleJsDepth：该选项允许你设置在 node_modules 文件夹中，编译器应该深入检查 JavaScript 文件的最大深度。

### Editor Support（编辑器支持）

提供编辑器相关的配置，使得编辑 TypeScript 更加高效

disableSizeLimit：这个配置项用于禁用 TypeScript 编译器对项目大小的限制。TypeScript 默认对项目大小有限制，如果项目过大，编译器可能会拒绝处理。
plugins：plugins 配置项允许你指定一个插件数组，这些插件可以在编译过程中提供额外的功能。这些插件通常用来扩展或增强 TypeScript 的标准功能，例如进行代码格式化、添加更丰富的类型检查，或者集成其他工具。

### Interop Constraints（互操作的约束）

确保 TypeScript 与其他 JavaScript 代码的互操作性

allowSyntheticDefaultImports：这个选项设置为 true 时，可以允许你对不是默认导出(default export )的模块使用默认导入语法。
esModuleInterop：类似于 allowSyntheticDefaultImports，这个选项提供了对 ES 模块和 CommonJS 模块之间更全面的互操作性。当设置为 true 时，它启用了 allowSyntheticDefaultImports 和其他一些互操作性行为。
forceConsistentCasingInFileNames：当这个配置开启时，确保你不能在导入时非一致地引用相同的文件。这可以避免在对大小写敏感和不敏感的文件系统之间移动代码时出现问题。
isolatedModules：开启该选项后，每个文件都被作为独立模块进行类型检查。这通常用于确保每个文件可以安全地隔离，适用于 Babel 这样的转译工具，它们只能处理单个文件。
preserveSymlinks：如果你使用符号链接指向模块，启用这个选项可以让 TypeScript 遵照符号链接的真实路径进行模块解析，而不是解析到符号链接的路径。

### Backwards Compatibility（向后兼容性）

处理 TypeScript 向后兼容性的配置选项

charset：这个选项用于指定在读取文件时使用的字符编码，确保 TypeScript 编译器正确解码源文件。只有当你的项目有特定编码需求时才需要改变默认值。
keyofStringsOnly：在启用时，keyof 操作符将仅返回 string 类型的键，这会忽略那些 number 或 symbol 类型的键。这是为了保持与旧版本 TypeScript 的兼容性。
noImplicitUseStrict：TypeScript 文件的顶部默认会插入"use strict"; 来采用严格模式。如果你的代码库需要避免这种注入，可以设置这个选项为 true。
noStrictGenericChecks：此选项设置为 true 时，TypeScript 在处理泛型时将使用较少的约束性检查，使其在某些场景下与旧版本兼容。
out：该选项用于指定合并所有输出到单个文件。现代的 TypeScript 项目推荐使用 outFile 或 outDir 代替。
suppressExcessPropertyErrors：当对象字面量存在任何“目标类型”里没有定义的属性时，通常 TypeScript 会报错。开启此选项会忽略这些额外的属性错误。
suppressImplicitAnyIndexErrors：默认情况下，TypeScript 不允许索引器隐式返回 any 类型，此选项开启后，会抑制这类错误。

### Language and Environment（语言与环境）

配置 TypeScript 语言和运行环境的相关设置

emitDecoratorMetadata：当使用装饰器时，这个选项会增加设计时类型元数据的发出，这用于某些反射场景，如 Angular 的依赖注入。
experimentalDecorators：启用实验性的装饰器支持，装饰器是 TypeScript 为类和类成员提供额外元数据的一种声明方式。
jsx：用于指定生成哪种类型的 JSX 代码，例如"react"，"preserve"，"react-native"等。
jsxFactory：当使用 jsx 时，指定生成 JSX 元素时使用的工厂函数名称。例如，React 应用通常使用 React.createElement。
jsxFragmentFactory：用于当创建 Fragment 类 jsx 元素时，指定要使用的工厂函数。
jsxImportSource：用于指定 jsx 转译时引入 jsx 工厂函数的模块。
lib：指定编译过程中需要包含的库文件列表，这可以让你根据目标环境选择正确的内置类型定义，如 DOM、ES2015 + 等。
moduleDetection：此选项不是 TypeScript 官方配置文档中的一部分，可能是已废弃的或特定工具的配置选项。
noLib：设置为 true 时，将不包括默认库文件（比如 lib.d.ts，包含标准 JavaScript 运行环境的类型等）。
reactNamespace：指定 React.createElement 由哪个对象提供。这在使用拥有不同命名空间的 React 克隆时很有用。
target：指定 ECMAScript 目标版本，例如 ES5、ES2015（ES6）、ES2017 等，决定了编译后代码的语法级别。
useDefineForClassFields：决定是否使用 defineProperty 来定义类字段，这与类字段的提案兼容。

### Compiler Diagnostics（编译器诊断）

配置编译器产生的诊断信息。

diagnostics：当此选项开启时，编译器会输出诊断信息，如编译过程中的时间和内存使用统计。
explainFiles：通过开启这个选项，你可以让编译器输出包含在编译中的文件及它们被包含的原因。
extendedDiagnostics：启用这个选项后，编译器将提供更为详尽的诊断信息，比如更多的时间消耗细节和内存使用情况。
generateCpuProfile：这个配置允许编译器生成一个 CPU profile，通常在“.cpuprofile”文件格式中，这有助于分析编译性能问题。
listEmittedFiles：开启后，编译器在编译过程结束后会列出所有生成的文件。
listFiles：开启此选项，编译器会在编译开始前输出所有将要编译的文件列表。
traceResolution：当这个选项开启时，会追踪模块解析的细节，并输出信息到控制台，有助于调试模块解析相关的问题。

### Projects（项目）

关于 TypeScript 项目配置的内容

composite：设定为 true 时，允许该项目作为其他项目的依赖项编译。这个选项主要用于构建大型项目，提升构建效率。
disableReferencedProjectLoad：如果设置为 true，在解析项目引用时, TypeScript 将不会加载引用的项目。这在有大量项目引用时可以改善编译速度。
disableSolutionSearching：此选项关闭了 TypeScript 的解决方案搜索功能，阻止它自动搜索基于项目引用的解决方案。
disableSourceOfProjectReferenceRedirect：如果开启此选项，TypeScript 编译器在构建项目时将不会使用源项目的输出文件，而是直接使用其源文件。
incremental：配置项设为 true 时，TypeScript 编译器将会生成增量编译信息，并在之后的编译中使用这些信息以缩短编译时间。
tsBuildInfoFile：用于指定存储增量编译信息的文件的位置，默认是.tsbuildinfo。

### Output Formatting（输出格式）

控制编译输出的格式

noErrorTruncation：此配置项设置为 true 时，TypeScript 编译器在报告错误时不会截断消息。这可以帮助你获得完整的错误信息，尤其是在错误信息非常长的时候。
preserveWatchOutput：当这个选项开启时，在监视模式下重新编译时，TypeScript 编译器不会清屏。这意味着你可以看到历史编译结果。
pretty：默认为 true，这个选项开启时，TypeScript 编译器会使用样式化和彩色的输出来展示错误信息等，使其更易于阅读。

### Completeness（完整性）

保证 TypeScript 配置的完整性，以及相关设置

skipDefaultLibCheck：如果设置为 true，编译器将跳过默认库（比如 lib.d.ts）的类型检查。当你确定默认库不会出问题时，这个选项可以减少编译时间。
skipLibCheck：与 skipDefaultLibCheck 类似，但这个选项会跳过所有声明文件（.d.ts files）的类型检查。这在你信任你的声明文件或者需要加快编译速度时非常有用。

### Command Line Watch Options（命令行监视选项）

配置在命令行中启用监视模式的选项

assumeChangesOnlyAffectDirectDependencies：设置为 true 时，TypeScript 编译器将假设文件改动仅影响到直接依赖的文件。在大型项目中，这可以显著加速编译过程，尤其是在你做的改动只影响少数文件时。

## Watch Options（监视选项）

与 tsc --watch 命令效果一样

watchFile：这个选项用于确定 TypeScript 如何监视单个文件的改动。你可以指定多种策略，比如使用文件系统的通知事件或者轮询等。
watchDirectory：类似于 watchFile，这个选项用来确定 TypeScript 如何监视文件夹的改动。它同样支持多种监视策略。
fallbackPolling：如果默认的文件监视机制不可用，这个选项指定了 TypeScript 应该如何使用轮询来监视文件和目录。在一些环境中，文件系统事件可能不够可靠，此时轮询可以作为后备选项。
synchronousWatchDirectory：设置为 true 时，监视目录的操作将同步执行。默认情况下，目录监视是异步的。这个选项对于性能和正确性的影响因操作系统和项目大小而异。
excludeDirectories：这个选项允许你指定一个目录列表，这些目录将被监视过程中排除。这在你的项目中有非代码相关目录或者很大的目录时特别有用，可以提升监视性能。
excludeFiles：类似于 excludeDirectories，但这是用来指定不需要监视的文件列表。通过排除特定的文件，你可以避免不必要的编译，节省时间。

## Type Acquisition（类型获取）

typeAcquisition 是一个专门针对自动类型获取配置的选项。

enable：当设置为 true 时，开启自动的类型获取。这允许 TypeScript 尝试自动下载和包含 @types 包，这些包包含了对应 JavaScript 库的类型定义。这对使用 JavaScript 的现有项目转向 TypeScript 非常有帮助，因为你不需要手动安装类型声明文件。
include：这个数组允许你指定要包含在类型获取过程中的库。即使你没有明确安装这些库的类型定义，TypeScript 也将尝试获取它们的类型声明。
exclude：如果某些库的类型声明会导致问题，或者你不希望 TypeScript 自动包含它们，可以在这个数组里指定这些库，以排除它们的自动类型获取。
disableFilenameBasedTypeAcquisition：设置为 true 时，TypeScript 将不会根据在代码中使用的模块名或路径尝试自动获取类型信息。默认情况下，如果项目中使用了某个 JavaScript 库，TypeScript 会尝试根据文件名识别库的类型定义。如果你关闭这个选项，TypeScript 将不执行这一行为。
