### 命令行选项

* --all 显示所有编译器选项。
* --build, -b 构建一个或多个项目及其依赖关系。
* --help, -h 打印此消息。
* --init 初始化 TypeScript 项目并创建一个 tsconfig.json 文件。
* --listFilesOnly 打印作为编译一部分的文件的名称，然后停止处理。
* --locale 设置 TypeScript 消息的语言。
* --project, -p 编译给定其配置文件路径的项目，或者带有 'tsconfig.json' 的文件夹。
* --showConfig 打印最终配置而不是构建。
* --version, -v 打印编译器的版本。
* --watch, -w 观察输入文件。

### 模块

* --allowArbitraryExtensions 启用对任何扩展名的文件的导入，前提是存在声明文件。类型: 布尔值，默认值: false
* --allowImportingTsExtensions 允许导入包含 TypeScript 文件扩展名。要求设置'* --moduleResolutionbundler'并设置'* --noEmit'或'* --emitDeclarationOnly'。类型: 布尔值，默认值: false
* --allowUmdGlobalAccess 允许从模块访问 UMD 全局变量。类型: 布尔值，默认值: false
* --baseUrl 指定解析非相对模块名称的基目录。
* --customConditions 在解析导入时设置解析器特定默认值以外的条件。
* --module, -m 指定生成哪种模块代码。其中之一: none, commonjs, amd, umd, system, es6 / es2015, es2020, es2022, esnext, node16, nodenext，默认值: 未定义
* --moduleResolution 指定 TypeScript 如何从给定模块说明符查找文件。其中之一: classic, node10 / node, node16, nodenext, bundler，默认值: 如果 module 是 AMD、UMD、System 或 ES6，则为 Classic，否则为 Node
* --moduleSuffixes 在解析模块时要搜索的文件名后缀列表。
* --noResolve 禁止'import'、'require'或'<reference>'扩展 TypeScript 应该添加到项目中的文件数量。类型: 布尔值，默认值: false
* --paths 指定一组条目，它们将导入重新映射到其他查找位置。默认值: 未定义
* --resolveJsonModule 启用对.json 文件的导入。类型: 布尔值，默认值: false
* --resolvePackageJsonExports 使用 package.json 中的'exports'字段解析包导入时。类型: 布尔值，默认值:当'moduleResolution'为'node16'、'nodenext'或'bundler'时为 true；否则为 false。
* --resolvePackageJsonImports 使用 package.json 中的'imports'字段解析导入时。类型: 布尔值，默认值:当'moduleResolution'为'node16'、'nodenext'或'bundler'时为 true；否则为 false。
* --rootDir 指定源文件中的根文件夹。类型: 字符串，默认值: 由输入文件列表计算而得
* --rootDirs 当解析模块时，允许将多个文件夹视为一个文件夹。一种或多种: 字符串，默认值: 由输入文件列表计算而得
* --typeRoots 指定多个文件夹，它们像'./node_modules/@types'一样起作用。
* --types 指定要包含的类型包名，而无需在源文件中引用它们。
JavaScript 相关支持
* --allowJs 允许 JavaScript 文件成为程序的一部分。使用'checkJS'选项从这些文件中获得错误。类型:布尔值，默认值:false
* --checkJs 在经过类型检查的 JavaScript 文件中启用错误报告。类型:布尔值默认项:false
* --maxNodeModuleJsDepth 指定用于检查'node_modules'中 JavaScript 文件的最大文件夹深度。只适用于'allowJs'。类型:数字，默认值:0

### 互操作性约束

* --allowSyntheticDefaultImports 允许 'import x from y'当模块没有默认导出时。类型: 布尔值, 默认值:如果 module 是"system"或 esModuleInterop 时为 true
* --esModuleInterop 发出额外的 JavaScript 以便于支持导入 CommonJS 模块。这使'allowSyntheticDefaultImports'可以进行类型兼容性。类型: 布尔值, 默认值: false
* --forceConsistentCasingInFileNames 确保在导入时大小写正确。类型: 布尔值, 默认值: true
* --isolatedModules 确保每个文件可以安全地进行转译，而不依赖于其他导入。类型: 布尔值, 默认值: false
* --preserveSymlinks 禁止解析链接到它们的真实路径。这与 node 中的相同标志相关。类型: 布尔值, 默认值: false
* --verbatimModuleSyntax 不转换或省略任何未标记为仅类型的导入或导出，确保它们根据'module'设置以输出文件的格式写入。类型: 布尔值, 默认值: false

### 类型检查

* --allowUnreachableCode 禁用对无法访问代码的错误报告。类型: 布尔值，默认值: 未定义
* --allowUnusedLabels 禁用对未使用标签的错误报告。类型: 布尔值，默认值: 未定义
* --alwaysStrict 确保始终发出'usestrict'。类型: 布尔值，默认值: 如果设置了 strict 则为 false。
* --exactOptionalPropertyTypes 按原样解释可选属性类型，而不是添加'undefined'。类型: 布尔值，默认值: false
* --noFallthroughCasesInSwitch 启用对 switch 语句中贯穿情况的错误报告。类型: 布尔值，默认值: false
* --noImplicitAny 启用对带有隐含的'any'类型的表达式和声明的错误报告。类型: 布尔值，默认值: 如果设置了 strict 则为 false。
* --noImplicitOverride 确保派生类中的覆盖成员带有 override 修饰符。类型: 布尔值，默认值: false
* --noImplicitReturns 启用对在函数中没有明确返回的代码路径的错误报告。类型: 布尔值，默认值: false
* --noImplicitThis 启用当'this'被赋予'any'类型时的错误报告。类型: 布尔值，默认值: 如果设置了 strict 则为 false。
* --noPropertyAccessFromIndexSignature 强制使用索引访问器键入时使用索引类型声明的键。类型: 布尔值，默认值: false
* --noUncheckedIndexedAccess 在使用索引时，向类型添加'undefined'。类型: 布尔值，默认值: false
* --noUnusedLocals 启用对未读取本地变量的错误报告。类型: 布尔值，默认值: false
* --noUnusedParameters 在函数参数未被读取时引发错误。类型: 布尔值，默认值: false
* --strict 启用所有严格的类型检查选项。类型: 布尔值，默认值: false
* --strictBindCallApply 检查'bind'、'call'和'apply'方法的参数是否与原函数匹配。类型: 布尔值，默认值: 如果设置了 strict 则为 false。
* --strictFunctionTypes 在赋值函数时，检查以确保参数和返回值是子类型兼容的。类型: 布尔值，默认值: 如果设置了 strict 则为 false。
* --strictNullChecks 在类型检查时考虑'null'和'undefined'。类型: 布尔值，默认值: 如果设置了 strict 则为 false。
* --strictPropertyInitialization 检查在构造函数中声明但未设置的类属性。类型: 布尔值，默认值: 如果设置了 strict 则为 false。
* --useUnknownInCatchVariables 将 catch 子句变量默认为'unknown'而不是'any'。类型: 布尔值，默认值: 如果设置了 strict 则为 false。

### 观察和构建模式

* --assumeChangesOnlyAffectDirectDependencies 在使用'incremental'和'watch'模式进行重新编译时，假设文件中的更改只会影响直接依赖它的文件。类型: 布尔值，默认值: false
向后兼容性
* --charset 不再支持。在早期版本中，手动设置读取文件的文本编码。类型:字符串，默认值:utf8
* --keyofStringsOnly 使 keyof 只返回字符串而不是字符串、数字或符号。这是一个旧选项。类型:布尔值，默认值:false
* --noImplicitUseStrict 禁止在输出 JavaScript 文件中添加'usestrict'指令。类型:布尔值，默认值:false
* --noStrictGenericChecks 禁止对函数类型中的泛型签名进行严格检查。类型:布尔值，默认值:false
* --out 已弃用的设置。使用'outFile'代替。
* --suppressExcessPropertyErrors 禁用在创建对象字面量期间报告过多属性错误。类型:布尔值，默认值:false
* --suppressImplicitAnyIndexErrors 抑制缺少索引签名的对象索引时'noImplicitAny'错误。类型:布尔值，默认值:false

### 项目相关

* --composite 启用约束，以便将 TypeScript 项目用于项目引用。类型: 布尔值，默认值: false
* --disableReferencedProjectLoad 减少 TypeScript 自动加载的项目数量。类型: 布尔值，默认值: false
* --disableSolutionSearching 当编辑时选择项目退出多项目引用检查。类型: 布尔值，默认值: false
* --disableSourceOfProjectReferenceRedirect 禁用在引用复合项目时优先使用源文件而非声明文件。类型: 布尔值，默认值: false
* --incremental, -i 保存.tsbuildinfo 文件，以实现项目的增量编译。类型: 布尔值，默认值: 当设置了 composite 时为 false。
* --tsBuildInfoFile 指定.tsbuildinfo 增量编译文件的路径。类型: 字符串，默认值:.tsbuildinfo

### 生成文件相关

* --declaration, -d 从项目中的 TypeScript 和 JavaScript 文件生成.d.ts 文件。类型: 布尔值，默认值: 当设置 composite 时为 false。
* --declarationDir 指定生成声明文件的输出目录。
* --declarationMap 为 d.ts 文件创建 sourcemaps。类型: 布尔值，默认值: false
* --downlevelIteration 发出更符合标准但冗长且性能不佳的 JavaScript 进行迭代。类型: 布尔值，默认值: false
* --emitBOM 在输出文件的开始处发出 UTF - 8 字节顺序标记（BOM）。类型: 布尔值，默认值: false
* --emitDeclarationOnly 只输出 d.ts 文件而不是 JavaScript 文件。类型: 布尔值，默认值: false
* --importHelpers 允许每个项目只从 tslib 导入一次辅助函数，而不是在每个文件中包含它们。类型: 布尔值，默认值: false
* --importsNotUsedAsValues 指定对仅用于类型的导入的发射 / 检查行为。其中之一: remove, preserve, error，默认值: remove
* --inlineSourceMap 在发出的 JavaScript 中包括 sourcemap 文件。类型: 布尔值，默认值: false
* --inlineSources 在发出的 JavaScript 中包括源码以及 sourcemaps。类型: 布尔值，默认值: false
* --mapRoot 指定调试器应该在何处查找源文件而不是生成位置的 map 文件。
* --newLine 为发出文件设置换行符。其中之一: crlf, lf
* --noEmit 禁用从编译中发射文件。类型: 布尔值，默认值: false
* --noEmitHelpers 禁止在编译输出中生成自定义辅助函数，例如'\_\_extends'。类型: 布尔值，默认值: false
* --noEmitOnError 禁止在报告类型检查错误时发射文件。类型: 布尔值，默认值: false
* --outDir 为所有发射文件指定一个输出文件夹。
* --outFile 指定将所有输出合并到一个 JavaScript 文件的文件。如果'declaration'为 true，也指定一个文件，该文件将所有.d.ts 输出捆绑到一起。
* --preserveConstEnums 禁止在生成的代码中擦除'constenum'声明。类型: 布尔值，默认值: false
* --preserveValueImports 在 JavaScript 输出中保留本来会被删除的未使用的导入值。类型: 布尔值，默认值: false
* --removeComments 禁止在输出文件中发出注释。类型: 布尔值，默认值: false
* --sourceMap 为发出的 JavaScript 文件创建 sourcemaps。类型: 布尔值，默认值: false
* --sourceRoot 指定调试器在何处查找参考源代码的根路径。
* --stripInternal 禁止发射包含'@internal'JSDoc 注释的声明。类型: 布尔值，默认值: false

### 编译器诊断

* --diagnostics 在构建后输出编译器性能信息。
* --explainFiles 打印包括编译中包含的文件以及包含原因。
* --extendedDiagnostics 在构建后输出更详细的编译器性能信息。
* --generateCpuProfile 为调试输出编译器运行的 v8CPU 配置文件。类型:字符串，默认值:profile.cpuprofile
* --generateTrace 生成事件跟踪和类型列表。
* --listEmittedFiles 在编译后打印发出的文件名。
* --listFiles 打印在编译过程中读取的所有文件。
* --traceResolution 记录'moduleResolution'过程中使用的路径。类型:布尔值，默认值:false

### 编辑器支持

* --disableSizeLimit 移除 TypeScript 语言服务器中用于 JavaScript 文件的总源代码大小 20mb 上限。类型: 布尔值，默认值: false
* --plugins 指定要包括的语言服务插件列表。默认值: 未定义

### 语言和环境

* --emitDecoratorMetadata 在源文件中为装饰的声明发出设计类型元数据。类型: 布尔值，默认值: false
* --experimentalDecorators 启用对传统实验性装饰器的实验性支持。类型: 布尔值，默认值: false
* --jsx 指定生成什么 JSX 代码。其中之一: preserve, react, react - native, react - jsx, react - jsxdev，默认值: 未定义
* --jsxFactory 指定当目标为 ReactJSX 发出时使用的 JSX 工厂函数，例如'React.createElement'或'h'。类型: 字符串，默认值: React.createElement
* --jsxFragmentFactory 指定用于片段的 JSXFragment 引用，当目标为 ReactJSX 发出时，例如'React.Fragment'或'Fragment'。类型: 字符串，默认值: React.Fragment
* --jsxImportSource 指定在使用'jsx:react-jsx\*'时用来导入 JSX 工厂函数的模块说明符。类型: 字符串，默认值: react
* --lib 指定一组描述目标运行环境的打包库声明文件。一种或多种: es5, es6 / es2015, es7 / es2016, es2017, es2018, es2019, es2020, es2021, es2022, es2023, esnext, dom, dom.iterable, webworker, webworker.importscripts, webworker.iterable, scripthost, es2015.core, es2015.collection, es2015.generator, es2015.iterable, es2015.promise, es2015.proxy, es2015.reflect, es2015.symbol, es2015.symbol.wellknown, es2016.array.include, es2017.date, es2017.object, es2017.sharedmemory, es2017.string, es2017.intl, es2017.typedarrays, es2018.asyncgenerator, es2018.asynciterable / esnext.asynciterable, es2018.intl, es2018.promise, es2018.regexp, es2019.array, es2019.object, es2019.string, es2019.symbol / esnext.symbol, es2019.intl, es2020.bigint / esnext.bigint, es2020.date, es2020.promise, es2020.sharedmemory, es2020.string, es2020.symbol.wellknown, es2020.intl, es2020.number, es2021.promise / esnext.promise, es2021.string, es2021.weakref / esnext.weakref, es2021.intl, es2022.array, es2022.error, es2022.intl, es2022.object, es2022.sharedmemory, es2022.string / esnext.string, es2022.regexp, es2023.array / esnext.array, es2023.collection / esnext.collection, esnext.intl, esnext.disposable, esnext.decorators, decorators, decorators.legacy，默认值: 未定义
* --moduleDetection 控制用哪种方法来检测模块格式的 JS 文件。其中之一: legacy, auto, force，默认值: "auto"：将带有 import，exports，import.meta，jsx(withjsx: react - jsx)，或 esm 格式的文件(withmodule: node16 +)视为模块。
* --noLib 禁用包括任何库文件，包括默认的 lib.d.ts。类型: 布尔值，默认值: false
* --reactNamespace 指定'createElement'调用的对象。这仅适用于目标为'react'JSX 发出时。类型: 字符串，默认值: React
* --target, -t 设置发出的 JavaScript 的语言版本并包括兼容的库声明。其中之一: es3, es5, es6 / es2015, es2016, es2017, es2018, es2019, es2020, es2021, es2022, esnext，默认值: es5
* --useDefineForClassFields 发出符合 ECMAScript 标准的类字段。类型: 布尔值，默认值: 对于 ES2022 和更高版本，包括 ESNext，为 true。

### 输出格式化

* --noErrorTruncation 在错误消息中禁用类型截断。类型:布尔值，默认值:false
* --preserveWatchOutput 在观察模式下禁用清除控制台。类型:布尔值，默认值:false
* --pretty 启用 TypeScript 输出的颜色和格式化，以使编译器错误更易于阅读。类型:布尔值，默认值:true

### 完整性

* --skipDefaultLibCheck 跳过检查与 TypeScript 一起提供的.d.ts 文件的类型。类型: 布尔值，默认值: false
* --skipLibCheck 跳过检查所有.d.ts 文件的类型。类型: 布尔值，默认值: false

### 热更新选项

* --watch, -w 将启动对当前项目文件更改的观察。
* --watchFile 指定 TypeScript 观察模式的工作方式。其中之一: fixedpollinginterval, prioritypollinginterval, dynamicprioritypolling, fixedchunksizepolling, usefsevents, usefseventsonparentdirectory，默认值: usefsevents
* --watchDirectory 在缺乏递归文件监视功能的系统上，指定监视目录的方式。其中之一: usefsevents, fixedpollinginterval, dynamicprioritypolling, fixedchunksizepolling，默认值: usefsevents
* --fallbackPolling 指定如果系统耗尽了本机文件监视器，观察者应该使用什么方法。其中之一: fixedinterval, priorityinterval, dynamicpriority, fixedchunksize，默认值: priorityinterval
* --synchronousWatchDirectory 在不支持本机递归监视的平台上，同步调用回调并更新目录监视器状态。类型: 布尔值，默认值: false
* --excludeDirectories 从观察过程中删除一组目录。
* --excludeFiles 从观察模式的处理中删除一组文件。

### 构建选项

* --build, -b 将使 tsc 表现得更像构建协调器而非编译器。
* --verbose, -v 启用详细日志记录。
* --dry, -d 展示将要构建（或者和'* --clean'配合使用时删除）的内容。
* --force, -f 构建所有项目，包括那些看起来是最新的项目。
* --clean 删除所有项目的输出。
