const elemName = "my-custom-element";
const ele = document.querySelector(elemName);
const slots = document.querySelector("#slots");
class MyCustomElement extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
    this.shadowRoot.appendChild(slots);
  }
}
customElements.define(elemName, MyCustomElement);
