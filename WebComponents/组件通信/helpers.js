// 组件工厂
const createCustomElement = (config) => {
  const {
    name,
    attrs = [],
    mode = "open",
    temp,
    parent = document.body,
    BaseClass = HTMLElement,
  } = config;
  let elem = document.createElement(name);
  customElements.define(
    name,
    // @ts-ignore
    class extends BaseClass {
      constructor() {
        super();
        this.initProxy();
        this.attachShadow({ mode });
        this.shadowRoot?.appendChild(temp.content);
      }
      initProxy() {
        // 监听设置标签属性操作
        elem = new Proxy(elem, {
          set: (target, property, value) => {
            target.setAttribute(property, value);
            return Reflect.set(target, property, value);
          },
          get: (target, property) => {
            const obj = Reflect.get(target, property);
            // 事件函数优化，取this指向问题
            if (typeof obj === "function") return obj.bind(this);
            return obj;
          },
        });
      }
      attributeChangedCallback(attrName, oldValue, newValue) {
        console.log(`${attrName}属性的旧值：${oldValue}，新值：${newValue}`);
      }
      static observedAttributes = attrs;
    }
  );
  parent.appendChild(elem);
  return elem;
};
// 简单的响应式全局状态
class GlobalState {
  constructor(state, action) {
    this.state = this.initState(state);
    this.action = action;
  }
  initState(state) {
    return new Proxy(state, {
      set: (target, key, val) => {
        Reflect.set(target, key, val);
        this.action(target);
        return true;
      },
    });
  }
}
