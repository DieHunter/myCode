import Utils, { createElement } from "./node_modules/utils-lib-js/dist/esm/index.js"
import type { MessageCenter as IMessageCenter } from "utils-lib-js"
const MessageCenter: typeof IMessageCenter = Utils.eventMessageCenter
// 菜单子项
type MenuListItem = {
    label: string
    name?: string
    handler?(e: MouseEvent): void
}
// DOM标签类型
type ICustomElementItem = Partial<HTMLElement & EventTarget> | null
type IParentElem = HTMLElement | void | null
// style对象属性类型
type IStyleItem = string | number | CSSRule | ((index: number) => string) | ((property: string, value: string | null, priority?: string | undefined) => void) | null | undefined
// 空函数，避免抛错
const noop = () => void 0
/**
 * 自定义标签的基类
 */
class BaseElem extends MessageCenter {
    root: HTMLElement = document.body
    remove(ele: IParentElem) {
        ele?.parentNode?.removeChild(ele)
    }
    moveTo({ x, y }: { x?: number, y?: number }, ele: IParentElem) {
        if (!ele) return
        ele.style.left = `${x}px`
        ele.style.top = `${y}px`
    }
}
/**
 * 菜单类
 */
export class Menu extends BaseElem {
    constructor(public menuList: MenuListItem[] = [], public menu?: HTMLElement) {
        super()
        this.root.addEventListener("contextmenu", this.menuHandler)
    }
    /**
     * 创建菜单函数
     * @param e 
     */
    menuHandler = (e: MouseEvent) => {
        e.preventDefault();// 取消默认事件
        this.remove(this.menu)
        this.create(this.root)
        this.moveTo({
            x: e.clientX,
            y: e.clientY
        }, this.menu)
        this.renderMenuList()
    }
    /**
     * 创建菜单元素
     * @param parent 父元素
     */
    create(parent: HTMLElement) {
        this.menu = createElement({
            ele: "ul",
            attr: { id: "menu" },
            parent
        })
    }
    /**
     * 菜单列表
     * @param list 列表数据
     * @param parent 父元素
     * @returns 
     */
    renderMenuList(list: MenuListItem[] = this.menuList, parent: IParentElem = this.menu) {
        if (!parent) return
        list.forEach(it => this.renderMenuListItem(it, parent))
    }
    /**
     * 菜单列表子项
     * @param item 单个列表数据
     * @param parent 父元素
     * @returns 列表子项
     */
    renderMenuListItem(item: MenuListItem, parent: HTMLElement) {
        const li = createElement({
            ele: "li",
            attr: {
                textContent: item.label
            },
            parent
        })
        li.addEventListener("click", item.handler ?? noop)
        return item
    }
}
/**
 * 自定义的元素类
 */
export class CustomElement extends BaseElem {
    selectClass = "custom-box"// 未被选中标签class值
    private _selectEle: ICustomElementItem = null// 当前选中的标签
    count: number = 0// 计数器，区分标签
    constructor() {
        super()
        document.onselectstart = () => false// 取消文字选中
    }
    /**
     * 选中标签后的样式变化
     */
    set selectEle(val: ICustomElementItem) {
        const { _selectEle } = this
        this.resetEleClass()
        if (val && val !== _selectEle) {
            val.className = `select ${this.selectClass}`
            this._selectEle = val
        }
    }
    get selectEle() {
        return this._selectEle
    }
    /**
     * 初始化事件
     * @param ele 
     */
    initEve = (ele: HTMLElement) => {
        ele.addEventListener("dblclick", this.select)
    }
    /**
     * 复制标签时增加复制文本标识
     * @param elem 
     */
    setCount(elem: HTMLElement) {
        elem.textContent += "(copy)"
        ++this.count
    }
    /**
     * 选中标签后重置上一个标签的样式
     * @returns 
     */
    resetEleClass() {
        if (!this._selectEle) return
        this._selectEle.className = this.selectClass
        this._selectEle = null
    }
    /**
     * 新建标签
     * @returns 标签对象
     */
    create() {
        const ele = createElement({
            ele: "div",
            attr: { className: this.selectClass, textContent: (++this.count).toString() },
            parent: this.root
        })
        return ele
    }
    /**
     * 初始化标签
     * @param e 鼠标事件
     * @param elem 标签对象
     */
    add(e: MouseEvent, elem?: HTMLElement) {
        const ele = elem ?? this.create()
        ele && this.initEve(ele)
        this.moveTo({
            x: e.clientX,
            y: e.clientY
        }, ele)
    }
    /**
     * 复制标签操作
     * @param e 鼠标事件
     * @returns 
     */
    cloneNode(e: MouseEvent) {
        if (!this.selectEle) return
        const _elem = this.selectEle?.cloneNode?.(true) as HTMLElement
        _elem && this.root.appendChild(_elem)
        _elem && this.setCount(_elem)
        this.add(e, _elem)
        this.selectEle = _elem
    }
    /**
     * 删除标签
     * @returns 
     */
    removeEle() {
        if (!this.selectEle) return
        this.remove(this.selectEle as IParentElem)
        this.selectEle = null
        --this.count
    }
    /**
     * 选中/取消选中标签
     * @param e 
     */
    select = (e: MouseEvent) => {
        this.selectEle = e.target
    }
    /**
     * 点击body时取消选中（未使用）
     * @param e 
     */
    unselected = (e: MouseEvent) => {
        if (e.target === this.root) this.selectEle = null
    }
}
/**
 * 拖拽基类
 */
class BaseDrag extends BaseElem {
    constructor(public elem: HTMLElement, public root: any = document) {
        super()
        this.init()
    }
    /**
     * 初始化事件
     */
    init() {
        this.elem.onmousedown = this.__mouseHandler//添加点击事件,避免重复定义
    }
    /**
     * 将一些公共函数在基类中实现
     * @param e 事件对象
     */
    private __mouseHandler = (e: Partial<MouseEvent>) => {
        const { type } = e
        if (type === "mousedown") {
            this.root.addEventListener("mouseup", this.__mouseHandler);
            this.root.addEventListener("mousemove", this.__mouseHandler);
        } else if (type === "mouseup") {
            this.root.removeEventListener("mouseup", this.__mouseHandler);
            this.root.removeEventListener("mousemove", this.__mouseHandler);
        }
        type && this.emit(type, e)// 触发子类的函数，进行后续操作
    }
    /**
     * 取消拖拽
     */
    reset() {
        this.elem.onmousedown = null
    }
}
/**
 * 拖拽调整标签位置
 */
export class Drag extends BaseDrag {
    offset?: Partial<{ x: number, y: number }>// 鼠标点击时在元素上的位置
    constructor(public elem: HTMLElement) {
        super(elem)
        this.on("mousedown", this.mouseHandler)
        this.on("mousemove", this.mouseHandler)
    }
    /**
     * 鼠标事件处理函数，当鼠标按下时，记录鼠标点击时在元素上的位置；当鼠标移动时，根据鼠标位置的变化计算新的位置，并通过调用父类的moveTo方法来移动元素
     * @param e 
     */
    mouseHandler = (e: Partial<MouseEvent>) => {
        const { type, target, clientX = 0, clientY = 0 } = e
        if (type === "mousedown") {
            this.offset = {
                x: e.offsetX,
                y: e.offsetY
            }
        } else if (type === "mousemove") {
            const { x = 0, y = 0 } = this.offset ?? {}
            this.moveTo({
                x: clientX - x,
                y: clientY - y
            }, target as HTMLElement)
        }
    }
}


/**
 * 拖拽调整标签尺寸
 */
export class Resize extends BaseDrag {
    startX?: number
    startY?: number
    startWidth?: IStyleItem
    startHeight?: IStyleItem
    constructor(public elem: HTMLElement) {
        super(elem)
        this.on("mousedown", this.mouseHandler)
        this.on("mousemove", this.mouseHandler)
    }
    /**
     * 获取标签样式项
     * @param ele 标签
     * @param key 样式属性名
     * @returns 样式属性值
     */
    getStyle(ele: Element, key: keyof CSSStyleDeclaration) {
        const styles = document.defaultView?.getComputedStyle?.(ele)
        if (styles && typeof styles[key] === "string") return parseInt(styles[key] as string, 10)
    }
    /**
     * 鼠标事件处理函数，用于处理鼠标按下和移动事件。当鼠标按下时，记录起始位置和当前宽度、高度的值。当鼠标移动时，根据鼠标位置的变化计算新的宽度和高度，并更新元素的样式。
     * @param e 
     */
    mouseHandler = (e: Partial<MouseEvent>) => {
        const { type, clientX = 0, clientY = 0 } = e
        if (type === "mousedown") {
            this.startX = clientX;
            this.startY = clientY;
            this.startWidth = this.getStyle(this.elem, "width")
            this.startHeight = this.getStyle(this.elem, "height")
        } else if (type === "mousemove") {
            const width = <number>this.startWidth + (clientX - <number>this.startX);
            const height = <number>this.startHeight + (clientY - <number>this.startY);
            this.elem.style.width = width + 'px';
            this.elem.style.height = height + 'px';
        }
    }
}