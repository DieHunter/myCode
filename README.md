### 介绍

{**简介**
该仓库是一个案例仓库，里面存放了一些作者发布在博客上的案例及项目的集合，当前仓库以整合为主，后续可能将案例分模块单独展示
博客地址：http://website.diehunter1024.work/blog_website/
}

### 技术栈

主要以 JS 为主，商城项目包含 Vue，React，Node，后续将使用 TS 等

### 使用说明

1.  git init
2.  git clone git@gitee.com:DieHunter/myCode.git
3.  第一次需要初始化子模块 git submodule init（重要）
4.  目前部分代码已经以子模块的形式迁出，可以通过 git submodule update --remote 来更新子模块（重要）
5.  fork了本仓库的用户可以通过 git submodule add 仓库地址 来添加子模块，如git submodule add https://gitee.com/DieHunter/test.git

### 参与贡献

1.  Fork 本仓库
2.  Star 本仓库

### 博客地址

#### CSDN：https://blog.csdn.net/time_____/
#### 博客园：https://www.cnblogs.com/HelloWorld-Yu/
#### 稀土掘金：https://juejin.cn/user/2225067265365966/posts

### 源码仓库

#### 码云：https://gitee.com/DieHunter
#### GitHub：https://github.com/DieHunter1024

### 书签备份

#### 地址：http://website.diehunter1024.work/bookmarks/index.html

### 案例

#### 零食商贩：http://website.diehunter1024.work/website-shopping-web/#/Home
#### 零食商贩管理系统：http://website.diehunter1024.work/website-shopping-manage/#/login

### 自动化平台

#### Jenkins：http://jenkins.diehunter1024.work/
#### Npm：http://npm.diehunter1024.work/

### 联系方式（请备注来意）

#### 微信：DieHunter1024
#### QQ：313267717
#### 公众号：阿宇的编程之旅