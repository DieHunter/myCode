/*
 * @Author: Hunter
 * @Date: 2023-07-10 17:46:35
 * @LastEditTime: 2023-08-02 16:05:10
 * @LastEditors: Hunter
 * @Description:
 * @FilePath: \北sen\main.js
 * 可以输入预定的版权声明、个性签名、空行等
 */
var appName = "北森iTalent", //app名
  packageName = getPackageName(appName), //包名
  roundTimer = 60 * 1000, //超时定时器间隔60秒
  randomTimer = parseInt(Math.random() * 5 * 60 * 1000), //随机定时器0-5分钟（精确到毫秒）
  maxRetryCount = 3, //重试打卡次数
  useEmail = true, // 是否发送邮件
  useDate = true, // 是否检查节假日
  timeThread = null, // 流程超时检测线程
  cardMenuBtn = () => id("mIVBottomCenter").findOne().bounds(), //打卡界面菜单
  cardViewBtn = () => text("签到").findOne().bounds(), //打卡界面按钮
  positionBtn = () => id("tv_sign_company_status").text("办公地点"), //定位成功按钮
  retryPositionBtn = () =>
    id("tv_match_des").textContains("刷新重试").findOne().bounds(), //重新定位按钮
  cardTakeBtn = () => id("rlt_sign_click").findOne(), //打卡按钮
  cardListBtn = () => id("recycler_sign_list").findOne(); //打卡记录列表
let logs = ``;
// 日志记录
const __log = function () {
  logs += `
  ${new Date()}：${JSON.stringify(arguments)}
  `;
  toast(JSON.stringify(arguments));
  console.trace.apply(null, arguments);
};
const getLog = () => logs;
const mailApi = "https://api.emailjs.com/api/v1.0/email/send", //邮箱请求地址
  mailConfig = {
    user_id: "user_xxxxxxxxxxxxxxxxxxxxxxhj",
    service_id: "sexxxxxxxmk",
    template_id: "templxxxxxxxxxxmn",
    accessToken: "8xxxxxxxxxxxxxxxxxxxxxxxxxxx9",
    template_params: {
      title: "自动打卡通知",
      content: `打卡成功
      日志:${getLog()}
      `,
      email: "xxxxxxxxx@qq.com", // 接收消息的邮箱
    },
  }, //邮箱配置，需要去emailjs官网申请api，每月免费200次
  dateApi = "http://api.tianapi.com/jiejiari/index", //节假日接口
  dateConfig = {
    key: "9dxxxxxxxxxxxxxxxxxx93",
    date: formatDate(new Date()),
  }; //在天行数据申请节假日api（每天免费查询100次）：https://www.tianapi.com/
__log("随机延迟时间：", randomTimer);
if (useDate) {
  // 检查是否开启节假日检测
  checkDateIsWork(dateConfig, function (res) {
    if (res.newslist[0].isnotwork) {
      sendEmail(setNewMessage("今天是法定节假日，无需打卡"));
      exitApp(true);
      return;
    }
    exitApp(false);
    setTimeout(init, randomTimer);
  });
} else {
  exitApp(false);
  setTimeout(init, randomTimer);
}

function init() {
  if (!!maxRetryCount) {
    __log("剩余重试次数" + maxRetryCount);
    timeOutMsg();
    maxRetryCount--;
    startProgram();
    return;
  }
  exitApp(true);
}
//开启应用
function startProgram() {
  __log("launchApp:" + appName, launchApp(appName)); //打开app
  waitForPackage(packageName); //等待app打开
  __log("launchAppSuccess", packageName);
  openCardView();
}
//首页--->打卡页
function openCardView() {
  var cardMenuButton = cardMenuBtn();
  __log(
    "打卡界面菜单",
    click(cardMenuButton.centerX(), cardMenuButton.centerY())
  );
  var cardButton = cardViewBtn();
  __log("进入打卡界面", click(cardButton.centerX(), cardButton.centerY() - 10));
  takeCard();
}
//打卡
function takeCard() {
  const timer = createSingleThread(null, retryPosition); // 重新定位
  __log("等待定位");
  positionBtn().waitFor();
  createSingleThread(timer);
  __log("定位成功");
  __log("点击打卡按钮", cardTakeBtn().click());
  sleep(5000);
  checkHasCard();
  sendSuccEmail();
  exitApp(true);
}
// 重新定位子线程延时操作
function retryPosition() {
  __log("重新定位子线程");
  sleep(3000); // 3秒超时
  var retry = retryPositionBtn();
  __log("定位超时，重新定位", click(retry.centerX() + 20, retry.centerY()));
}
// 检查当天打卡记录
function checkHasCard() {
  cardListBtn()
    .children()
    .forEach(function (child, i) {
      __log(`打卡记录${i}:${child.children()[0].text()}`);
    });
}
// 打卡成功发邮件
function sendSuccEmail() {
  const _mailConfig = mailConfig;
  _mailConfig.template_params.content += getLog();
  __log("发送邮件", sendEmail(_mailConfig));
}
//退出程序
function exitApp(exitJs, fn) {
  shell("am force-stop " + packageName, true);
  fn && fn();
  exitJs && exit();
}

// 程序超时处理
function timeOutMsg() {
  createSingleThread(timeThread, function () {
    //在新线程执行的代码
    setTimeout(function () {
      sendEmail(setNewMessage("自动打卡超时，正在重试"));
      exitApp(false, init);
    }, roundTimer);
  });
}
// 创建子线程单例，避免重复创建同一个线程
function createSingleThread(thread, fn) {
  if (thread) {
    thread.interrupt();
  }
  if (fn) {
    thread = threads.start(fn);
  }
  return thread;
}
// 发送邮件api
function sendEmail(params) {
  if (useEmail) {
    var res = http.post(mailApi, params || mailConfig, {
      contentType: "application/json",
    });
    return res;
  }
}
// 节假日api请求
function checkDateIsWork(params, fn) {
  var res = http.post(dateApi, params || dateConfig).body.json();

  if (res.code === 200) {
    fn(res);
    return;
  }
  __log(res);
  sendEmail(setNewMessage(res.msg));
}
// 修改默认邮件提示信息
function setNewMessage(msg) {
  var _mailConfig = simpleCloneObj(mailConfig, {
    contentType: "application/json",
  });
  _mailConfig.template_params.content = `${msg + new Date()}
  日志:${getLog()}
  `;
  return _mailConfig;
}

//日期格式转换 YYYY-MM-DD
function formatDate(date) {
  var y = date.getFullYear();
  var m = date.getMonth() + 1;
  m = m < 10 ? "0" + m : m;
  var d = date.getDate();
  d = d < 10 ? "0" + d : d;
  return y + "-" + m + "-" + d;
}

// 简单的深复制
function simpleCloneObj(target) {
  return typeof target === "object" && JSON.parse(JSON.stringify(target));
}
