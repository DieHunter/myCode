// @ts-nocheck
const appName = "企业微信";
const msgName = ""; // 群聊天label
let packageName = "com.tencent.wework";
const Actives = {
  WwMainActivity: "com.tencent.wework.launch.WwMainActivity",
};
const init = () => {
  app.launchApp(appName);
  waitForActivity(Actives.WwMainActivity, 200);
  text(msgName).findOne().parent().click();
  text("参与接龙").findOne().parent().click();
  text("接龙")
    .find()
    .forEach(function (tv) {
      tv.click();
    });
  text("确定").findOne().click();
  exitApp();
};
//退出程序
function exitApp(exitJs, fn) {
  shell("am force-stop " + packageName, true);
  fn && fn();
  exitJs && exit();
}
init();
