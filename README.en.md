### Introduction

{** Introduction **
The warehouse is a case warehouse, which stores a collection of cases and projects published by some authors on the blog. The current warehouse is mainly integrated, and the cases may be displayed separately by modules in the future
Blog: http://website.diehunter1024.work/blog_website/
}

### Technology stack

Mainly based on JS, the mall project includes Vue, React, Node, and will use TS in the future

### Instructions for use

1.  git init
2.  git clone git@gitee.com:DieHunter/myCode.git
3.  git submodule init (important)
4.  At present, part of the code has been migrated out in the form of submodules, you can update the submodules through git submodule update --remote (important)

### Contribute

1. Fork the local warehouse
2. Star Warehouse

### Blog address

# # # # CSDN:https://blog.csdn.net/time_____/

Garden: # # # # blog https://www.cnblogs.com/HelloWorld-Yu/

# # # # re the nuggets: https://juejin.cn/user/2225067265365966/posts

### Source code repository

# # # # code cloud: https://gitee.com/DieHunter

# # # # GitHub:https://github.com/DieHunter1024

### Bookmark backup

# # # # address: http://website.diehunter1024.work/bookmarks/index.html

### Case

# # # # snack vendor: http://website.diehunter1024.work/website-shopping-web/#/Home

# # # # snack vendor management system: http://website.diehunter1024.work/website-shopping-manage/#/login

### Automation platform

# # # # Jenkins: http://jenkins.diehunter1024.work/

# # # # Npm: http://npm.diehunter1024.work/
