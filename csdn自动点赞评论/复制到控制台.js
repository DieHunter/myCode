let index = 0; //第几条博客
const maxPage = 100; //博客总数
const page = 3; //5页停止一次，防止检测评论太快
const time = 30 * 1000; //延时评论
const speed = 5 * 1000; //评论速度
let timeTick; //计时器
const elements = document
  .querySelector(".Community")
  .querySelectorAll(".Community-item-active"); //博客列表
init();

function init() {
  timeTick = setInterval(function () {
    if (index > maxPage) {
      clearInterval(timeTick);
      return;
    }
    if (index && index % page == 0) {
      clearInterval(timeTick);
      setTimeout(init, time);
    }
    const hrefItem = elements[index++].querySelector("a").href;
    window.open(hrefItem, "_blank");
  }, speed);
}

function keyDownCtrl() {
  const mockKeyboardEvent = new KeyboardEvent("keydown", {
    ctrlKey: true,
  });
  document.dispatchEvent(mockKeyboardEvent);
}
document.onkeypress = function (params) {
  console.log("ctrl");
};
