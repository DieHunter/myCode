const { createRandom } = require("./utils/helper.random");
const { setCrypto } = require("./utils/helper.crypto");
const { messageCenter } = require("event-message-center");
const { initServer } = require("./server");
const { catchAwait } = require("utils-lib-js");
let __token = null;
// 登录信息
const userInfo = {
  username: "zhangsan",
  password: "123123",
};
// 初始化服务端
initServer();
// 客户端加密混淆操作
const encryption = ({ query = {}, key = "params", token = __token }) => {
  query.id = createRandom(); //生成随机id混淆参数
  query.token = token;
  return {
    [key]: setCrypto({ data: query }).toString(),
  };
};
// 模拟前端用户登录操作
const userLogin = (query) => {
  const params = encryption({ query });
  return messageCenter.invoke("/login", params);
};
// 模拟登录成功后请求
const getInfo = (query) => {
  const params = encryption({ query });
  return messageCenter.invoke("/info", params);
};
// 初始化函数
const init = async () => {
  const [err, res] = await catchAwait(userLogin(userInfo));
  if (err) return console.error(err);
  __token = res.token;
  const [err2, info] = await catchAwait(getInfo());
  if (err2) return console.error(err2);
  console.log(info);
};
init();
