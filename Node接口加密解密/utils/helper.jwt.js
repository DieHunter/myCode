const { sign, verify } = require("jsonwebtoken");
const { defer } = require("utils-lib-js");
const { TokenKey } = require("../config");
// 新建令牌
exports.createToken = ({
  payload = {},
  tokenKey = TokenKey,
  expiresIn = "1d",
  ...others
}) => {
  return sign({ payload }, tokenKey, {
    expiresIn,
    ...others,
  });
};
// 校验令牌
exports.checkToken = ({ token, tokenKey = TokenKey, options }) => {
  const { reject, resolve, promise } = defer();
  verify(token, tokenKey, options, (err, decoded) => {
    if (err) return reject(err);
    return resolve(decoded.payload);
  });
  return promise;
};
