const bcryptjs = require("bcryptjs");
// 哈希盐加密
exports.createBcrypt = (password, salt = bcryptjs.genSaltSync(10)) => {
  return bcryptjs.hashSync(password, salt);
};
// 校验密码
exports.checkBcrypt = (_password, _hash) => {
  return bcryptjs.compareSync(_password, _hash);
};
