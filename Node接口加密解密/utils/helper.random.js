const { randomNum } = require("utils-lib-js");
// 生成Nonce随机数
exports.createRandom = () => {
  const date = new Date().getTime();
  const start = 0x000000;
  const end = 0xffffff;
  return randomNum(start, end) + date;
};
