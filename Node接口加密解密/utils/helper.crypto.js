const cryptoJS = require("crypto-js");
const { CryptoKey } = require("../config");
const { jsonToString, stringToJson } = require("utils-lib-js");
const defaultOpt = {
  mode: cryptoJS.mode.ECB,
  padding: cryptoJS.pad.Pkcs7,
};
const __key = CryptoKey; // 加密关键字
// 加密
const setCrypto = ({ data, key = __key, opts = defaultOpt }) => {
  return cryptoJS.AES.encrypt(jsonToString(data), key, opts);
};
// 解密
const getCrypto = ({
  str,
  key = __key,
  resToStr = true,
  opts = defaultOpt,
}) => {
  str = decodeURIComponent(str); //前端传参有特殊字符（中文）时转义（替换百分号）
  const bytes = cryptoJS.AES.decrypt(str, key, opts);
  const source = bytes.toString(cryptoJS.enc.Utf8);
  return resToStr ? stringToJson(source) : source;
};
module.exports = {
  setCrypto,
  getCrypto,
};
