const { messageCenter } = require("event-message-center");
const { defer, catchAwait } = require("utils-lib-js");
const { getCrypto } = require("./utils/helper.crypto");
const { createBcrypt, checkBcrypt } = require("./utils/helper.bcrypt");
const { createToken, checkToken } = require("./utils/helper.jwt");
const __temp = new Map(); // 将请求过的id存起来（后续可以加定时任务清除缓存，或者增加长度限制）
const userInfo = {
  // 指代数据库取数据
  username: "zhangsan",
  password: createBcrypt("123123"),
};
// 解密操作
const decrypt = (query) => {
  return getCrypto({ str: query });
};
// 请求去重
const checkRepeat = (query = {}) => {
  const __id = query.id;
  if (!!!__id || __temp.has(__id)) return;
  return __temp.set(__id, query);
};
// 抛错
const promiseRej = (err) => Promise.reject(err);
// 加个简单的中间件，做校验
const middleware = {
  decrypt: (data) => {
    // 解密，重复请求校验
    const { resolve, reject, promise } = defer();
    const params = decrypt(data.params);
    if (!!!checkRepeat(params)) reject("重复请求或id为空");
    else resolve(params);
    return promise;
  },
  token: async ({ token, ...data }) => {
    // token校验
    const { resolve, reject, promise } = defer();
    const [err, username] = await catchAwait(checkToken({ token }));
    if (err) reject("token过期或失效");
    else resolve({ ...data, username });
    return promise;
  },
  checkPassword: async (data) => {
    // 密码校验
    const { resolve, reject, promise } = defer();
    if (!!!checkBcrypt(data.password, userInfo.password)) reject("密码错误");
    else resolve(data);
    return promise;
  },
  chackUser: async (data) => {
    // 用户校验
    const { resolve, reject, promise } = defer();
    if (data.username !== userInfo.username) reject("没找到用户");
    else resolve(data);
    return promise;
  },
};

exports.initServer = () => {
  messageCenter.watch("/login", async (data) => {
    const [err, params] = await catchAwait(middleware.decrypt(data));
    const [err2, params2] = await catchAwait(middleware.checkPassword(params));
    if (err || err2) return promiseRej(err ?? err2);
    return { token: createToken({ payload: params2.username }) };
  });
  messageCenter.watch("/info", async (data) => {
    const [err, params] = await catchAwait(middleware.decrypt(data));
    const [err2, params2] = await catchAwait(middleware.token(params));
    const [err3, params3] = await catchAwait(middleware.chackUser(params2));
    if (err || err2 || err3) return promiseRej(err ?? err2 ?? err3);
    return { msg: "获取成功", username: params3.username };
  });
};
